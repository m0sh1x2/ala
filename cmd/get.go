/*
Copyright © 2021 Krasimir Velichkov krasimirvelichkov@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/m0sh1x2/ala/src/log/mutations"
	"gitlab.com/m0sh1x2/ala/src/menu"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Gets the top 15 logs for a specified type of output",
	Long: `Example usage:
	get EdgeIP 15   	-- Gets top 15 EdgeIPs
	get OriginIP 15   	-- Gets top 15 OriginIPs
	get UserAgent 5 	-- Gets top 15 UserAgents
	`,
	Run: func(cmd *cobra.Command, args []string) {
		// s := spinner.New(spinner.CharSets[14], 80*time.Millisecond) // Build our new spinner
		// s.Start()                                                   // Start the spinner
		// allLogs := []string{}

		// We are expecting 3 inputs logs, type and maxRange
		// If they are not present then we can just open a menu

		if len(args) < 3 {
			// viper.Set("logPath", ".")

			// panic("no no smaller sir")
			menu.GetMenu()

			// We don't need to proceed when we are in the menu
			return
		}
		// if (args[1] && args[1] && args[2]) != false {

		// }

		// The second argument is topIPNum which is an integer
		topIpNum, err := strconv.Atoi(args[2])
		if err != nil {
			panic("Can't convert string to int")
		}
		// fmt.Println(args[0])

		// We are not type checking the args[0]
		for _, logs := range mutations.GetTopType(args[0], args[1], topIpNum) {
			fmt.Printf("%d - %s \n", logs.Occurance, logs.IP)
			// fmt.Println(logs.IP + "  " + logs.Occurance)
		}

		// fmt.Println(logmutations.GetTopType("../combined_logs_for_processing.txt", "EdgeIP", 10))
		// s.Stop()
		// fmt.Println(allLogs)
	},
}

func init() {
	rootCmd.AddCommand(getCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
