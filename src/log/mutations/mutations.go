package mutations

import (
	"fmt"
	"regexp"
	"sort"
	"strings"

	"github.com/fatih/color"
	"gitlab.com/m0sh1x2/ala/src/log/actions"
	logactions "gitlab.com/m0sh1x2/ala/src/log/actions"
	"gitlab.com/m0sh1x2/ala/src/log/format"
)

type KV struct {
	IP        string
	Occurance int
}

func GetFullLogAnalysis(logPath string, maxOutputLines int) {
	topTypes := []string{"QueryStrings", "RequestUri", "UserAgent", "ClientIP", "StatusCode", "RequestType"}
	for _, reqType := range topTypes {

		// Prints the req type with color
		lineSeparator := "========"
		fmt.Printf("%v %v %v  \n", color.CyanString(lineSeparator), color.GreenString(reqType), color.CyanString(lineSeparator))

		switch reqType {

		case "QueryStrings":
			for index, queryString := range GetTopQueryStrings(logPath) {
				fmt.Printf("%d - %s \n", queryString.Occurance, queryString.IP)
				if index >= maxOutputLines {
					break
				}
			}
		default:
			for _, result := range GetTopType(logPath, reqType, maxOutputLines) {
				fmt.Printf("%d - %s \n", result.Occurance, result.IP)
			}
		}

	}
}

// Get the Counter but you might be looking for the GetTopType function :)
func getTypeDistinctCounter(outputType string, logs []format.LogFormat) map[string]int {
	outputTypeResult := make(map[string]int)

	// TODO: Improve the switch statement to use type checking? - this looks horrible

	// Set up a map that will increase the value of each LogFormat value stored in the unordered list/map
	switch outputType {
	case "EdgeIP":
		for _, row := range logs {
			outputTypeResult[row.EdgeIP.IP]++
		}
	case "ClientIP":
		for _, row := range logs {
			outputTypeResult[row.ClientIP.IP]++
		}
	case "HttpProtocol":
		for _, row := range logs {
			outputTypeResult[row.HttpProtocol]++
		}
	case "UserAgent":
		for _, row := range logs {
			outputTypeResult[row.UserAgent]++
		}
	case "RequestType":
		for _, row := range logs {
			outputTypeResult[row.RequestType]++
		}
	case "RequestUri":
		for _, row := range logs {
			outputTypeResult[row.RequestUri]++
		}
	case "StatusCode":
		for _, row := range logs {
			outputTypeResult[row.StatusCode]++
		}
	case "RequestSize":
		for _, row := range logs {
			outputTypeResult[row.RequestSize]++
		}
	case "FullRequest":
		for _, row := range logs {
			outputTypeResult[row.FullRequest]++
		}
	case "PHPVersion":
		for _, row := range logs {
			outputTypeResult[row.PHPVersion]++
		}
	case "RequestTime":
		for _, row := range logs {
			outputTypeResult[row.RequestTime]++
		}
	default:
		fmt.Printf("Input method %v doesn't exist  \n", color.RedString(outputType))
	}
	return outputTypeResult
}

// TODO: Implement generic for the outputType that can be IP, UserAgent, Requests Size, Request URI
func GetTopType(logPath string, outputType string, topIpNum int) []KV {

	// Get the distinct counter based on the input/output type
	distinctCounter := getTypeDistinctCounter(outputType, logactions.ReadLog(logPath))

	// Create a sortable struct
	var ss []KV

	// Transform the map into the KV struct
	for k, v := range distinctCounter {
		ss = append(ss, KV{k, v})
	}

	// This is the basic sort implemented in v 1.18
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Occurance > ss[j].Occurance
	})

	var logResult []KV

	for index, value := range ss {
		logResult = append(logResult, value)
		if index == topIpNum {
			break
		}
	}
	return logResult
}

// Get all PHP requests with full LogFormat
func GetTopPHPCalls(logPath string, topRequests int) []format.LogFormat {
	initialLogs := actions.ReadLog(logPath)
	logResults := []format.LogFormat{}

	// sortedResults := []KV{}
	// Sort the logs and get all of the PHP Calls
	for index, log := range initialLogs {

		// TODO: Optimize the regex search
		isPHPLog, _ := regexp.MatchString("PHP", log.PHPVersion)
		if isPHPLog {
			logResults = append(logResults, log)
		}

		if index >= topRequests {
			break
		}
	}

	// for k, v := range logResults {
	// 	sortedResults = append(sortedResults, KV{k, v})
	// }

	// Get the ones that occur most regularly by RequestUri
	// for _, theLogs := range logResults {
	// 	fmt.Println(theLogs)
	// }
	return logResults
}

// TODO: This is slowing down the calculations so lets optimize it
func GetTopPHPRequestUri(logPath string, topRequestAmount int) []format.LogFormat {
	requestUriResult := []format.LogFormat{}

	// We are going through the TopPHPCalls
	for _, currentLogs := range GetTopPHPCalls(logPath, topRequestAmount) {
		fmt.Println(currentLogs.RequestUri)
	}

	return requestUriResult
}

// Get all QUery strings
// This method should return a sorted list with occurance of query strings of each element separated by ? an &
func GetTopQueryStrings(logPath string) []KV {

	var sortedQueryStrings []KV
	unsortedQueryStrings := map[string]int{}
	// Read the Access Logs
	initialLogs := actions.ReadLog(logPath)
	// Get the RequestURIs

	for _, logLine := range initialLogs {

		requestUri := logLine.RequestUri

		isQueryStrig, _ := regexp.MatchString("\\?", requestUri)

		// Count the unique QueryStrings
		if isQueryStrig {
			// Separate the query strings from the URL
			escapedRequestUri := strings.Split(requestUri, "?")[1]
			// Get all of the query strings
			extractedQueryStrings := strings.Split(escapedRequestUri, "&")
			// Count the strings in the map
			for _, escapedRange := range extractedQueryStrings {
				// Get the query string key and append it to the counter
				unsortedQueryStrings[strings.Split(escapedRange, "=")[0]]++
			}
		}
	}
	// Sort the query strings by value
	for k, v := range unsortedQueryStrings {
		sortedQueryStrings = append(sortedQueryStrings, KV{k, v})
	}

	sort.Slice(sortedQueryStrings, func(i, j int) bool {
		return sortedQueryStrings[i].Occurance > sortedQueryStrings[j].Occurance
	})

	// Sort the query strings and count them up

	// Return the sorted Query Strings with most occurance
	return sortedQueryStrings
}
