/*

Generates logs based on the LogFormat struct.

Examples:
- High PHP calls
- Well-Known user-agents or unknown ones
- Unusual file/request size


*/

package generator
