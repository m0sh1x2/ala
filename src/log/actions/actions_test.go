package actions_test

import (
	"gitlab.com/m0sh1x2/ala/src/log/format"
)

type TestCase struct {
	value  []format.LogFormat
	expect []format.LogFormat
	actual []format.LogFormat
}

// TODO: Fix and implement the test

// func TestReadLog(t *testing.T) {
// 	// We are just reading the file
// 	logPath := "./logformat_test.log"
// 	testLogs, err := os.Open(logPath)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	expectedOutput := `{85.236.155.146 17703}
// 	{5.254.55.37 7214}
// 	{37.166.55.52 4303}
// 	{63.143.42.246 2889}
// 	{82.142.7.152 1764}
// 	{213.175.162.54 857}`

// 	testCase := TestCase{
// 		value:    format.GetFormattedLog(testLogs),
// 		expected: expectedOutput,
// 	}

// 	testCase.actual = logactions.ReadLog(logPath)
// 	if testCase.actual != testCase.expect {
// 		t.Error("Inputted: {}, Received: {}, Expected {}", testCase.value, testCase.actual, testCase.expected)
// 	}
// 	// We have to format the log as if it is from a file
// }
