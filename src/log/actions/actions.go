package actions

import (
	"bufio"
	"log"
	"os"
	"strings"

	"gitlab.com/m0sh1x2/ala/src/log/format"
)

// Gets formatted logs with the LogFormat struct
func ReadLog(logPath string) []format.LogFormat {
	file, err := os.Open(logPath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	// TODO: Optimize line breaking and for loop
	formattedLogs := []format.LogFormat{}

	for scanner.Scan() {
		currentScan := strings.Split(scanner.Text(), "\"")
		formattedLogs = append(formattedLogs, format.GetFormattedLog(currentScan))
	}

	return formattedLogs
}
