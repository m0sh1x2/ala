package format_test

import (
	"strings"
	"testing"

	"gitlab.com/m0sh1x2/ala/src/log/format"
)

type TestCase struct {
	value    []string
	expected format.LogFormat
	actual   format.LogFormat
}

// TODO: The basic formatting might be improved or every type can be tested
func TestGetFormattedLogCronjob(t *testing.T) {
	t.Run("Test log 1", func(t *testing.T) {
		testString := `8.8.8.8 1.1.1.1 - [06/Dec/2021:00:42:49 +0000] "POST /wp-cron.php?doing_wp_cron=1638751369.3646619319915771484375 HTTP/1.1" 200 0 "https://google.com/wp-cron.php?doing_wp_cron=1638751369.3646619319915771484375" "WordPress/5.6.6; https://google.com" "PHP/7.4.25" 0`
		testCase := TestCase{
			value: strings.Split(testString, "\""),
			expected: format.LogFormat{
				EdgeIP:       format.IP{"8.8.8.8"},
				ClientIP:     format.IP{"1.1.1.1"},
				Date:         "[06/Dec/2021:00:42:49",
				TimeZone:     "+0000]",
				RequestType:  "POST",
				RequestUri:   "/wp-cron.php?doing_wp_cron=1638751369.3646619319915771484375",
				HttpProtocol: "HTTP/1.1",
				StatusCode:   "200",
				RequestSize:  "0",
				FullRequest:  "https://google.com/wp-cron.php?doing_wp_cron=1638751369.3646619319915771484375",
				UserAgent:    "WordPress/5.6.6; https://google.com",
				PHPVersion:   `PHP/7.4.25`,
				RequestTime:  "0",
			},
		}
		testCase.actual = format.GetFormattedLog(testCase.value)
		if testCase.actual != testCase.expected {
			t.Error("Inputted: {}, Received: {}, Expected {}", testCase.value, testCase.actual, testCase.expected)
		}
	})

	t.Run("Test log 1", func(t *testing.T) {
		testString := `144.202.92.152 216.244.66.232 - [06/Dec/2021:01:02:46 +0000] "GET /mon-compte-le-colisee/ HTTP/1.1" 302 0 "-" "Mozilla/5.0 (compatible; DotBot/1.2; +https://opensiteexplorer.org/dotbot; help@moz.com)" "PHP/7.4.25" 2`
		testCase := TestCase{
			value: strings.Split(testString, "\""),
			expected: format.LogFormat{
				EdgeIP:       format.IP{"144.202.92.152"},
				ClientIP:     format.IP{"216.244.66.232"},
				Date:         "[06/Dec/2021:01:02:46",
				TimeZone:     "+0000]",
				RequestType:  "GET",
				RequestUri:   "/mon-compte-le-colisee/",
				HttpProtocol: "HTTP/1.1",
				StatusCode:   "302",
				RequestSize:  "0",
				FullRequest:  "-",
				UserAgent:    "Mozilla/5.0 (compatible; DotBot/1.2; +https://opensiteexplorer.org/dotbot; help@moz.com)",
				PHPVersion:   `PHP/7.4.25`,
				RequestTime:  "2",
			},
		}
		testCase.actual = format.GetFormattedLog(testCase.value)
		if testCase.actual != testCase.expected {
			t.Error("Inputted: {}, Received: {}, Expected {}", testCase.value, testCase.actual, testCase.expected)
		}
	})

	t.Run("Test log 3", func(t *testing.T) {
		testString := `108.61.87.100 66.249.66.94 - [06/Dec/2021:00:59:58 +0000] "GET /favicon.ico HTTP/1.1" 404 197 "-" "Googlebot-Image/1.0" "-" 0`
		testCase := TestCase{
			value: strings.Split(testString, "\""),
			expected: format.LogFormat{
				EdgeIP:       format.IP{"108.61.87.100"},
				ClientIP:     format.IP{"66.249.66.94"},
				Date:         "[06/Dec/2021:00:59:58",
				TimeZone:     "+0000]",
				RequestType:  "GET",
				RequestUri:   "/favicon.ico",
				HttpProtocol: "HTTP/1.1",
				StatusCode:   "404",
				RequestSize:  "197",
				FullRequest:  "-",
				UserAgent:    "Googlebot-Image/1.0",
				PHPVersion:   `-`,
				RequestTime:  "0",
			},
		}
		testCase.actual = format.GetFormattedLog(testCase.value)
		if testCase.actual != testCase.expected {
			t.Error("Inputted: {}, Received: {}, Expected {}", testCase.value, testCase.actual, testCase.expected)
		}
	})

	t.Run("Test log 4", func(t *testing.T) {
		testString := `185.107.95.169 144.76.40.25 - [06/Dec/2021:00:42:45 +0000] "GET /comments/feed/ HTTP/1.1" 304 0 "-" "Mozilla/5.0 (compatible; DataForSeoBot/1.0; +https://dataforseo.com/dataforseo-bot)" "PHP/7.4.25" 4`
		testCase := TestCase{
			value: strings.Split(testString, "\""),
			expected: format.LogFormat{
				EdgeIP:       format.IP{"185.107.95.169"},
				ClientIP:     format.IP{"144.76.40.25"},
				Date:         "[06/Dec/2021:00:42:45",
				TimeZone:     "+0000]",
				RequestType:  "GET",
				RequestUri:   "/comments/feed/",
				HttpProtocol: "HTTP/1.1",
				StatusCode:   "304",
				RequestSize:  "0",
				FullRequest:  "-",
				UserAgent:    "Mozilla/5.0 (compatible; DataForSeoBot/1.0; +https://dataforseo.com/dataforseo-bot)",
				PHPVersion:   `PHP/7.4.25`,
				RequestTime:  "4",
			},
		}
		testCase.actual = format.GetFormattedLog(testCase.value)
		if testCase.actual != testCase.expected {
			t.Error("Inputted: {}, Received: {}, Expected {}", testCase.value, testCase.actual, testCase.expected)
		}
	})

	t.Run("Test log 5", func(t *testing.T) {
		testString := `45.77.120.59 71.197.110.100 - [02/Feb/2020:23:18:48 -0600] "GET /wp-json/tcm/v1/comments/2233?itemsPerPage=20&page=1&sortBy=comment_karma&order=DESC&_=1580707107217 HTTP/1.1" 200 2945 "https://www.somesitedebug.com/sonnets/famous-sonnets/" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36" "PHP/7.2.26" 1753`
		testCase := TestCase{
			value: strings.Split(testString, "\""),
			expected: format.LogFormat{
				EdgeIP:       format.IP{"45.77.120.59"},
				ClientIP:     format.IP{"71.197.110.100"},
				Date:         "[02/Feb/2020:23:18:48",
				TimeZone:     "-0600]",
				RequestType:  "GET",
				RequestUri:   "/wp-json/tcm/v1/comments/2233?itemsPerPage=20&page=1&sortBy=comment_karma&order=DESC&_=1580707107217",
				HttpProtocol: "HTTP/1.1",
				StatusCode:   "200",
				RequestSize:  "2945",
				FullRequest:  "https://www.somesitedebug.com/sonnets/famous-sonnets/",
				UserAgent:    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
				PHPVersion:   `PHP/7.2.26`,
				RequestTime:  "1753",
			},
		}
		testCase.actual = format.GetFormattedLog(testCase.value)
		if testCase.actual != testCase.expected {
			t.Error("Inputted: {}, Received: {}, Expected {}", testCase.value, testCase.actual, testCase.expected)
		}
	})

	t.Run("Test log 6", func(t *testing.T) {
		testString := `45.77.120.59 68.190.236.201 - [02/Feb/2020:23:18:38 -0600] "POST /wp-admin/admin-ajax.php HTTP/1.1" 200 95 "https://www.yayaya.com/quotes/soliloquies/othello/" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36" "PHP/7.2.26" 5765`
		testCase := TestCase{
			value: strings.Split(testString, "\""),
			expected: format.LogFormat{
				EdgeIP:       format.IP{"45.77.120.59"},
				ClientIP:     format.IP{"68.190.236.201"},
				Date:         "[02/Feb/2020:23:18:38",
				TimeZone:     "-0600]",
				RequestType:  "POST",
				RequestUri:   "/wp-admin/admin-ajax.php",
				HttpProtocol: "HTTP/1.1",
				StatusCode:   "200",
				RequestSize:  "95",
				FullRequest:  "https://www.yayaya.com/quotes/soliloquies/othello/",
				UserAgent:    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36",
				PHPVersion:   `PHP/7.2.26`,
				RequestTime:  "5765",
			},
		}
		testCase.actual = format.GetFormattedLog(testCase.value)
		if testCase.actual != testCase.expected {
			t.Error("Inputted: {}, Received: {}, Expected {}", testCase.value, testCase.actual, testCase.expected)
		}
	})
}
