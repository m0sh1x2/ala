package format

import (
	"strings"
)

/*
Edge + Client + Date/Time - 108.61.197.203 5.254.55.37 - [06/Dec/2021:00:42:49 +0000]
- This can be split later ir directly in the struct?

Request - POST /wp-cron.php?doing_wp_cron=1638751369.3646619319915771484375 HTTP/1.1
- We need the post request and then the query strings from the URI - the HTTP version doesn't change but we must parse it as well

Status Code + Size - 200 0
- Critical as we must know if it is cached or not

Request URI - https://somesite.com/wp-cron.php?doing_wp_cron=1638751369.3646619319915771484375
- We can get the siteurl from here, should be the same as the request till the end

UserAgent - WordPress/5.6.6; https://somesite.com
- UserAgent is critical but too dynamic as we need to identify it if it is a bot/user or something else

PHP Version - PHP/7.4.25
- No rework required for this one

Time to execute - 0
- No rework required for this one as well
*/

type LogFormat struct {
	EdgeIP       IP
	ClientIP     IP
	Date         string
	TimeZone     string
	RequestType  string
	RequestUri   string
	HttpProtocol string
	StatusCode   string
	FullRequest  string
	UserAgent    string
	PHPVersion   string
	RequestSize  string
	RequestTime  string
}

type IP struct {
	IP string
}

func GetFormattedLog(logInput []string) LogFormat {
	partialLogData := strings.Split(logInput[0], " ")

	currentLogFormat := LogFormat{
		EdgeIP:       IP{partialLogData[0]},
		ClientIP:     IP{partialLogData[1]},
		Date:         partialLogData[3],
		TimeZone:     partialLogData[4],
		RequestType:  strings.Split(logInput[1], " ")[0],
		RequestUri:   strings.Split(logInput[1], " ")[1],
		HttpProtocol: strings.Split(logInput[1], " ")[2],
		StatusCode:   strings.Split(logInput[2], " ")[1],
		RequestSize:  strings.Split(logInput[2], " ")[2],
		FullRequest:  logInput[3],
		UserAgent:    logInput[5],
		PHPVersion:   logInput[7],
		RequestTime:  strings.Split(logInput[8], " ")[1],
	}
	return currentLogFormat
}

func GetIPs(logInput []string) {}
