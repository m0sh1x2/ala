package menu

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"time"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"github.com/manifoldco/promptui"
	"github.com/spf13/viper"
	"gitlab.com/m0sh1x2/ala/src/log/mutations"
	"gitlab.com/m0sh1x2/ala/src/sites"
)

type ProcessingOption struct {
	Name  string
	Value []mutations.KV // This is the cached result of the specific option
}
type SiteCachedData struct {
	Site         sites.Site
	CachedResult []ProcessingOption
}

func DrawBar(title string) {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	defer ui.Close()

	g2 := widgets.NewGauge()
	g2.Title = title
	g2.SetRect(0, 3, 50, 6)
	g2.Percent = 0
	g2.BarColor = ui.ColorYellow
	g2.LabelStyle = ui.NewStyle(ui.ColorBlue)
	g2.BorderStyle.Fg = ui.ColorWhite
	ui.Render(g2)

	uiEvents := ui.PollEvents()

	draw := func(count int) {
		g2.Percent = count % 101
		ui.Render(g2)
	}

	tickerCount := 1
	draw(tickerCount)
	tickerCount++
	ticker := time.NewTicker(time.Millisecond * 20).C

	for {
		select {
		case e := <-uiEvents:
			switch e.ID {
			case "q", "<C-c>":
				return
			}
		case <-ticker:
			draw(tickerCount)
			tickerCount++
			// if g2.Percent == 55 {
			// 	// TODO: Set up a list of easter egss with random string generation
			// 	g2.Title = "I am loosing millions $$$"
			// 	time.Sleep(time.Second)
			// } else if g2.Percent == 75 {
			// 	g2.Title = "Work FASTER!!!"
			// 	time.Sleep(time.Second)
			// } else if g2.Percent == 85 {
			// 	g2.Title = "Am I a joke to you?"
			// 	time.Sleep(time.Second)
			// } else
			if g2.Percent == 100 {
				return
			}

			// if g2.Percent == 69 {
			// 	time.Sleep(time.Second * 2)
			// } else if g2.Percent == 99 {
			// 	time.Sleep(time.Second)
			// } else if g2.Percent == 100 {
			// 	time.Sleep(time.Second)
			// 	return
			// }
		}
	}
}

// TODO: Set those variables in Viper for state management
// This can also live in a Closure
var selected = false
var selectedDomain sites.Site

func GetMenu() {

	processingOptions := []ProcessingOption{
		ProcessingOption{
			Name: "FullCheck",
		}, ProcessingOption{
			Name: "PHPRequests",
		}, ProcessingOption{
			Name: "QueryStrings",
		}, ProcessingOption{
			Name: "UserAgent",
		}, ProcessingOption{
			Name: "ClientIP",
		}, ProcessingOption{
			Name: "EdgeIP",
		}, ProcessingOption{
			Name: "RequestType",
		}, ProcessingOption{
			Name: "StatusCode",
		}, ProcessingOption{
			Name: "RequestUri",
		}, ProcessingOption{
			Name: "RequestSize",
		}, ProcessingOption{
			Name: "RequestTime",
		},
	}
	templates := &promptui.SelectTemplates{
		Label:    "{{ . }}?",
		Active:   "☢ {{ .Name | green }} ",
		Inactive: "  {{ .Name | cyan }}",
		Selected: "☢ {{ .Name | red | cyan }}",

		// TODO: Find a solution that doesn't break multi-line strings - https://github.com/manifoldco/promptui/issues/190
		// 		Details: `
		// ---------- Stats ----------
		// {{ "Name:" | faint }}	{{ .Name }}
		// {{ .Value }}
		// `,
	}

	// Initially we start with the menu and pick a domain
	// Once the domain is chosen we continue with the data processing
	maxOutputLines := 5
	if !selected {
		selectedDomain = SelectDomain()
		selected = true
	}

	// Fill up the processing options per domain
	// TODO: Set up caching per domain and cache ttl or force re-validate when needed
	// for index, option := range processingOptions {
	// 	// fmt.Println(option.Name)
	// 	if option.Name == "PHPRequests" {

	// 	} else if option.Name == "QueryStrings" {

	// 	} else if option.Name != "FullCheck" {
	// 		processingOptions[index].Value = mutations.GetTopType(viper.GetString("logPath"), option.Name, maxOutputLines)
	// 	}
	// }

	// fmt.Println(selectedDomain)

	// Once the domain is selected we go through each option and then process the data

	processingPrompt := promptui.Select{
		Label:     "Analyze logs of " + selectedDomain.Name,
		Items:     processingOptions,
		Templates: templates,
	}

	index, _, err := processingPrompt.Run()
	if err != nil {
		// panic(err)
		fmt.Println("Come back in 1000 years...")
		// GetMenu()
		SelectDomain()
	}

	if processingOptions[index].Name == "FullCheck" {
		// TODO: Optimize the DrawBar
		// DrawBar("Running Full Log analysis")

		mutations.GetFullLogAnalysis(viper.GetString("logPath"), 5)
	} else {

		// Loah logic in the menu of the app? TODO: decouple logic from the menu
		switch processingOptions[index].Name {
		case "PHPRequests":
			// This is basically the same as the RequestURI when loaded via PHP
			// TODO: Optimize and reduce code re-occurance
			for _, analyzedResult := range mutations.GetTopType(viper.GetString("logPath"), "RequestUri", maxOutputLines) {
				fmt.Printf("%d - %s \n", analyzedResult.Occurance, analyzedResult.IP)
			}
		case "QueryStrings":
			for index, queryStrings := range mutations.GetTopQueryStrings(viper.GetString("logPath")) {
				fmt.Printf("%d - %s \n", queryStrings.Occurance, queryStrings.IP)
				if index >= maxOutputLines {
					break
				}
			}
		default:
			for _, analyzedResult := range mutations.GetTopType(viper.GetString("logPath"), processingOptions[index].Name, maxOutputLines) {
				fmt.Printf("%d - %s \n", analyzedResult.Occurance, analyzedResult.IP)
			}
		}
	}
	GetMenu()
}

func ProcessOption(processingOptions ProcessingOption) {
	fmt.Println(processingOptions.Name)
}

func SelectDomain() sites.Site {
	clearScreen()

	templates := &promptui.SelectTemplates{
		Label:    "{{ . }}?",
		Active:   "☢ {{ .Name | green }} ({{ .AccessLogSize | red }})",
		Inactive: "  {{ .Name | cyan }} ({{ .AccessLogSize | red }})",
		Selected: "☢ {{ .Name | red | cyan }}",
		Details: `
--------- Stats ----------
{{ "Name:" | faint }}	{{ .Name }}
{{ "PHPCalls:" | faint }}	{{ .PHPCalls }}
{{ "Path:" | faint }}	{{ .Path }}
{{ "Access Logs:" | faint }}	{{ .AccessLogPath }}
{{ "Error Logs:" | faint }}	{{ .ErrorLogPath }}
{{ "Tickets:" | faint }}	{{ .Tickets }}
{{ "Tags:" | faint }}	{{ .Tags | red}}`,
	}

	domainsToSelect := sites.GetAllSites("./", viper.GetInt("searchDepth"))

	domainPrompt := promptui.Select{
		Label: "Select Site",
		// TODO: This might be a source of truth and it should pint to the strct names of the format.LogFormat struct
		// Items:     []string{"FullCheck", "PHPRequests", "QueryStrings", "UserAgent", "RequestUri", "ClientIP", "EdgeIP", "PHPVersion", "RequestType", "StatusCode", "RequestUri", "RequestSize", "RequestTime"},
		Items:     domainsToSelect,
		Templates: templates,
		CursorPos: 0,
		Size:      5,
	}
	// This is the default return value of the lines which every type analysis will return
	// TODO: Optimize the input of the value via the CLI or Options menu

	// We are getting the index of the domains slice as we will need only the name of the domain and no other data - except for log paths
	index, _, err := domainPrompt.Run()

	if err != nil {
		fmt.Println("Come back in 11 years...")
		os.Exit(1)
	}
	// We also need to set the logPath globally so that we can parse it in any other nested menu
	viper.Set("logPath", domainsToSelect[index].AccessLogPath)

	// We are returning the whole Site struct so that we can manage it in the next analysis function
	return domainsToSelect[index]
}

func clearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
