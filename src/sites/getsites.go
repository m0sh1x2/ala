package sites

import (
	"bytes"
	"io"
	"os"

	"github.com/asaskevich/govalidator"
)

type Site struct {
	Name          string
	AccessLogPath string
	AccessLogSize int // This is the total line numbers of the access logs
	ErrorLogPath  string
	ErrorLogSize  int // This is the total line numbers of the error logs
	PHPCalls      int
	Tags          []string
	Tickets       []string
	Path          string
}

// func GetRecursiveDirectory(dirPath string, depth int) []string {
// 	allDirs := []string{}

// 	initialDirs, err := os.ReadDir(dirPath)
// 	if err != nil {
// 		panic(err)
// 	}

// 	dirCounter := 0

// 	for _, dir := range initialDirs {
// 		// fullDirPath := dirPath + "/"

// 		allDirs = append(allDirs, dir.Name())
// 		if dirCounter >= depth {
// 			break
// 		}
// 		// if dir.Type().IsDir() {
// 		// 	// if dirCounter >= depth {
// 		// 	// 	break
// 		// 	// }
// 		// 	// TODO: This might cause issues based on the input - if doesn't contain a slash /
// 		// 	GetRecursiveDirectory(fullDirPath+dir.Name(), depth)

// 		// }
// 	}

// 	return allDirs
// }

// Read line numbers - https://stackoverflow.com/questions/24562942/golang-how-do-i-determine-the-number-of-lines-in-a-file-efficiently
// TODO: Test benchmarks on the line number reading
func LineCounter(r io.Reader) (int, error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}

// Gets and slice of the sites with their directories
func GetAllSites(searchDir string, depth int) []Site {

	sitesArray := []Site{}
	// In order to save the slice of sites we are using this recursive Closure - example/source - https://gobyexample.com/recursion
	var recursiveSites func(searchDir string, depth int) []Site

	recursiveSites = func(searchDir string, depth int) []Site {
		initialDirPath, _ := os.ReadDir(searchDir)
		for _, dir := range initialDirPath {
			if dir.Type().IsDir() {
				// Once we get in a directory we reduce the depth with one
				depth--
				// If the depth is a negative number then we break - this means that there might be more directories but we already have a specified depth
				if depth < 0 {
					break
				}
				nestedSearchDir := searchDir + dir.Name() + "/"

				if govalidator.IsURL(dir.Name()) {
					// Checks if the directory matches an URL or a valid domain
					// TODO: This might fail in some cases - empty dirs, nested directories with domains

					accessLogPath := nestedSearchDir + "logs/access_logs"

					inMemAccessBytes, _ := os.Open(accessLogPath)     // Open the file
					accessLogSize, _ := LineCounter(inMemAccessBytes) // and read the lines

					errorLogPath := nestedSearchDir + "logs/error_logs"

					sitesArray = append(sitesArray,
						Site{
							Name:          dir.Name(),
							PHPCalls:      5,
							AccessLogPath: accessLogPath,
							ErrorLogPath:  errorLogPath,
							AccessLogSize: accessLogSize,
							ErrorLogSize:  100,
							Tags:          []string{"cpu", "mysql"},
							Tickets:       []string{"#445522", "#112266"},
							Path:          nestedSearchDir,
						})
				}
				recursiveSites(nestedSearchDir, depth)
				// We are increasing the depth because there can be other directories in the same directory
				depth++
			}
		}
		return sitesArray
	}

	return recursiveSites(searchDir, depth)
}

// Ges one site

func GetSiteData() {

}
