package main

import (
	"github.com/spf13/viper"
	"gitlab.com/m0sh1x2/ala/cmd"
)

func main() {
	// logPath := "../combined_logs_for_processing.txt"

	// mutations.GetTopPHPRequestUri("../combined_logs_for_processing.txt", 5)

	// The search depth is very different from the prod directory depth

	// We do not need production environment variables
	viper.SetConfigFile(".env")

	// Find and read the config file
	err := viper.ReadInConfig()

	if err != nil {
		// log.Fatalf("Error while reading config file %s", err)
	}

	if viper.Get("ALA_DEV") == "1" {
		viper.Set("searchDepth", 5)
	} else {
		viper.Set("searchDepth", 3)
	}

	cmd.Execute()

	// sites := sites.GetAllSites("./", 5)

	// testFile := "test/production/domains/domain1.com/logs/access_logs"
	// testResult, _ := os.Open(testFile)
	// result, _ := sites.LineCounter(testResult)
	// fmt.Println(result)
	// fmt.Println(sites)
	// for _, site := range sites {
	// 	fmt.Println(site.Name)
	// }
	// fmt.Println(govalidator.IsURL("abv.zzz"))
	// menu.GetDomainsMenu()

	// fmt.Println(sites.GetRecursiveDirectory("./test/production/domains", 2))

	// sortedQueryStrings := mutations.GetTopQueryStrings("../combined_logs_for_processing.txt", 5)

	// for index, item := range sortedQueryStrings {
	// 	fmt.Println(item)

	// 	if index >= 10 {
	// 		break
	// 	}
	// }
}
