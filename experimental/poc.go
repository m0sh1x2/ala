package poc

import (
	"fmt"
	"log"
	"time"

	"github.com/briandowns/spinner"
	ui "github.com/gizak/termui/v3"
	"github.com/manifoldco/promptui"

	"github.com/gizak/termui/v3/widgets"
)

func DrawBar(targetResource string, targetType string) {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}
	defer ui.Close()

	g2 := widgets.NewGauge()
	g2.Title = "Analyzing " + targetType + " of " + targetResource
	g2.SetRect(0, 3, 50, 6)
	g2.Percent = 0
	g2.BarColor = ui.ColorYellow
	g2.LabelStyle = ui.NewStyle(ui.ColorBlue)
	g2.BorderStyle.Fg = ui.ColorWhite
	ui.Render(g2)

	uiEvents := ui.PollEvents()

	draw := func(count int) {
		g2.Percent = count % 101
		ui.Render(g2)
	}

	tickerCount := 1
	draw(tickerCount)
	tickerCount++
	ticker := time.NewTicker(time.Millisecond * 20).C

	for {
		select {
		case e := <-uiEvents:
			switch e.ID {
			case "q", "<C-c>":
				return
			}
		case <-ticker:
			draw(tickerCount)
			tickerCount++
			if g2.Percent == 69 {
				time.Sleep(time.Second * 2)
			} else if g2.Percent == 99 {
				time.Sleep(time.Second)
			} else if g2.Percent == 100 {
				time.Sleep(time.Second)
				return
			}
		}
	}
}

func main() {
	// logPath := "../combined_logs_for_processing.txt"

	// for _, log := range logmutations.GetTopType(logPath, "UserAgent", 50) {
	// 	fmt.Println(log)
	// }

	// chart := tm.NewLineChart(100, 20)

	// data := new(tm.DataTable)
	// data.AddColumn("Time")
	// data.AddColumn("Sin(x)")
	// data.AddColumn("Cos(x+1)")

	// for i := 0.1; i < 10; i += 0.1 {
	// 	data.AddRow(i, math.Sin(i), math.Cos(i+1))
	// }

	// tm.Println(chart.Draw(data))
	// // cmd.Execute()
	prompt := promptui.Select{
		Label: "Select Site",
		Items: []string{"All sites", "golangrocks.com", "waybetterthan.bash", "bashsucks.go", "stop.using.bash", "kartofi.bg",
			"kokazako.be", "abv.bg"},
	}

	_, result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}

	s := spinner.New(spinner.CharSets[14], 100*time.Millisecond) // Build our new spinner
	s.Start()                                                    // Start the spinner
	time.Sleep(1 * time.Second)                                  // Run for some time to simulate work
	s.Stop()
	fmt.Printf("Analyzing - %q\n", result)
	prompt2 := promptui.Select{
		Label: "What should I analyze?",
		Items: []string{"Full Log Analysis", "Top IPs", "UserAgents", "Top Dynamic Requests", "Query String", "CPU Usage",
			"Memory Usage"},
	}

	// _, result2, err := prompt2.Run()
	_, result2, err := prompt2.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}

	DrawBar(result, result2)

	fmt.Println("Vsi4ko tova go napravih za po-malko ot 30 minuti :)")
}
