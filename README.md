[![pipeline status](https://gitlab.com/m0sh1x2/ala/badges/main/pipeline.svg)](https://gitlab.com/m0sh1x2/ala/-/commits/main)

# Access Log Analyzer

- Concurent and very fast access log analyzer(this means that 9 pregnant ladies can deliver a baby in a month)
- Analyze parts of the logs
- Very friendly cli thanks to Cobra
- Written and fully tested in Go
- Display Cpu usage based on cGroup for the user only(TODO)
- Single binary to rule them all
- Anomaly detection
- Options for external event and state storage(log results)
- Send results to a centralized management storage for further reviews
- Compare other results and watch for anomalies
- Mark, Tag specific UserAgents, IP Addresses, and Query Strings - for anomaly detection or blacklists

# TODO

- Remote config save and pull
- Config can be for example - result line numbers, colors, options, or whatever
- Advanced menu(more processing time) - max data
- Simple menu(fast UI) with less data